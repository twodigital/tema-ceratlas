<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <?php wp_head(); ?>
    <!-- Global Site Tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-712819-13"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments)};
      gtag('js', new Date());

      gtag('config', 'UA-712819-13');
    </script>

  </head>
  <body id="page-body" <?php body_class(); ?> data-toggler="no-scroll">
  <?php do_action( 'foundationpress_after_body' ); ?>
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>

  <?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) === 'offcanvas' ) : ?>
  <div class="off-canvas-wrapper">
    <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
    <?php get_template_part( 'template-parts/mobile-off-canvas' ); ?>
  <?php endif; ?>

  <?php do_action( 'foundationpress_layout_start' ); ?>

  <header id="masthead" class="site-header" role="banner">
  <div class="top-bar">
    <div class="row">
      <div class="small-12 columns">
        <div class="top-bar-title">
          <span data-responsive-toggle="responsive-menu" data-hide-for="medium">
            <button  type="button" data-toggle><i class="fa fa-bars" aria-hidden="true"></i></button>
          </span>
          <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/identity/logo-ceramica-atlas.png" alt="Cerâmica Atlas"></a>
        </div>
        <div id="responsive-menu">
          <div class="top-bar-left">
          <nav role="navigation">
              <?php foundationpress_top_bar_r(); ?>
              <?php if ( ! get_theme_mod( 'wpt_mobile_menu_layout' ) || get_theme_mod( 'wpt_mobile_menu_layout' ) === 'topbar' ) : ?>
                <?php get_template_part( 'template-parts/mobile-top-bar' ); ?>
              <?php endif; ?>
            </nav>
          </div>
        </div>
        <div class="top-bar-right">
          <ul class="menu">
            <li><button id="dropdown-button" type="button" data-toggle="dropdown"><i class="fa fa-search" style="font-size: 1.4em; padding-top:8px" aria-hidden="true"></i></button>
            <div class="dropdown-pane large" id="dropdown" data-dropdown data-auto-focus="true">
              <form action="<?= esc_url(home_url())?>" method="GET" role="">
                <div class="row">
                    <div class="medium-7 columns">
                      <input type="text" class="expanded" name="s" placeholder="Buscar referência / nome" />
                    </div>
                    <div class="medium-5 columns">
                      <button type="submit" class="button expanded">Pesquisar</button>
                    </div>
                    <input type="hidden" name="post_type" value="produtos" />
                </div>
              </form>
            </div>
            </li>
            <li><button type="button" class="full-menu-toggler secondary hollow button" data-toggle="full-panel full-menu-toggler-icon page-body" ><i class="fa fa-bars" id="full-menu-toggler-icon" aria-hidden="true" data-toggler="fa-bars fa-times"></i></button></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="full-panel" style="display: none;" id="full-panel" data-toggler data-animate="fade-in fade-out">
    <div class="row expanded">
    <div class="medium-5 columns left-column">
    <div class="row">
      <div class="medium-6 columns">
        <?php foundationpress_full_left_nav(); ?>
        <hr>
        <address>
          <i class="fa fa-phone" aria-hidden="true"></i> <a class="link-phone" href="tel:+551936739600">+55 (19) 3673-9600</a><br>
          <i class="fa fa-envelope" aria-hidden="true"></i> <a class="link-email" href="mailto:faleconosco@ceratlas.com.br">faleconosco@ceratlas.com.br</a><br>
          <span class="address-text">Rodovia Padre Donizetti, SP 332, Km 272 - Tambaú/SP - CEP: 13.710-000</span><br>
          <a href="https://www.instagram.com/ceramicaatlas/" title="Instagram Cerâmica Atlas"><i class="fa fa-instagram" aria-hidden="true"></i></a> <a href="https://www.facebook.com/ceratlas" title="Facebook Cerâmica Atlas"><i class="fa fa-facebook-square" aria-hidden="true"></i></a> <span class="social-text">Nossas redes sociais</span>
        </address>
      </div>
    </div>

    </div>
    <div class="medium-7 columns right-column">
      <?php foundationpress_full_right_nav(); ?>
    </div>
    </div>
  </div>
  </header>
  <section class="container">
    <?php do_action( 'foundationpress_after_header' );
