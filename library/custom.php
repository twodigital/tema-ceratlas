<?php
if ( ! function_exists( 'formats_orderby' ) ) :
function formats_orderby( $orderby, $wp_query ) {
	global $wpdb;

	if ($wp_query->query['post_type'] == 'produtos' && $wp_query->query['disponibilidade'] == 'pronta-entrega' || $wp_query->query['disponibilidade'] == 'sob-encomenda') {
		$orderby = "(
			SELECT ca_terms.term_order
			FROM $wpdb->term_relationships
			INNER JOIN $wpdb->term_taxonomy USING (term_taxonomy_id)
			INNER JOIN $wpdb->terms USING (term_id)
			WHERE $wpdb->posts.ID = object_id
			AND taxonomy = 'formats'
			GROUP BY object_id
			ORDER BY $wpdb->posts.post_title
		) ";
		$orderby .= 'ASC';
	}

	return $orderby;
}
add_filter( 'posts_orderby', 'formats_orderby', 10, 2 );
endif;


if ( ! function_exists( 'wpsl_custom_templates' ) ) :
function wpsl_custom_templates( $templates ) {

    /**
     * The 'id' is for internal use and must be unique ( since 2.0 ).
     * The 'name' is used in the template dropdown on the settings page.
     * The 'path' points to the location of the custom template,
     * in this case the folder of your active theme.
     */
    $templates[] = array (
        'id'   => 'ceratlas',
        'name' => 'Template Ceratlas',
        'path' => get_stylesheet_directory() . '/' . 'template-parts/wpsl-custom.php',
    );

    return $templates;
}
endif;
add_filter( 'wpsl_templates', 'wpsl_custom_templates' );

if ( ! function_exists( 'change_template_for_search' ) ) :
// Pesquisa personalizada para produtos
function change_template_for_search($template)
{
  global $wp_query;
  $post_type = get_query_var('post_type');
  if( $wp_query->is_search && $post_type == 'produtos' )
  {
    return locate_template('archive-search-custom.php');
  }
  return $template;
}
endif;
add_filter('template_include', 'change_template_for_search');


if ( ! function_exists( 'register_form_ambiente' ) ) :
function register_form_ambiente() {
  // Valida campos vazios
  if(empty($_POST['titulo']) || empty($_POST['ambiente-categoria']) || empty($_POST['produto-utilizado']) || empty($_FILES['imagens']['name'][0]) ){
    wp_redirect(site_url().'/envie-seu-projeto?message="empty"');
    exit();
  }

  $my_post = array(
    'post_title'    => wp_strip_all_tags( $_POST['titulo'] ),
    'post_content'  => '',
    'post_status'   => 'pending',
    'post_author'   => 1,
    'post_type'     => 'aplicacoes'
  );



  $id_post = wp_insert_post( $my_post );
  if ( !is_numeric( $id_post ) ) {
    wp_redirect(site_url().'/envie-seu-projeto?message="error"');
    exit();
  }

  $idImage= array();

    $files = $_FILES["imagens"];

    foreach ($files['name'] as $key => $value) {
            if ($files['name'][$key]) {

              $ext = end(explode('.',$files['name'][$key]));

              $formats = array('png','jpg','jpeg');

              if (!in_array($ext, $formats)) {
                wp_delete_post( $id_post,true);
                wp_redirect(site_url().'/envie-seu-projeto?message="error-image-ext"');
          exit();
              }

              if ($files['size'][$key] > 2000000) {
                wp_delete_post( $id_post,true);
                wp_redirect(site_url().'/envie-seu-projeto?message="error-image"');
          exit();
              }

              $tamanhoValida = getimagesize($files['tmp_name'][$key]);
              if ($tamanhoValida[0] < 785 || $tamanhoValida[1] < 615 ) {
                wp_delete_post( $id_post,true);
                wp_redirect(site_url().'/envie-seu-projeto?message="error-image-tamanho"');
          exit();
              }


              $filename = $files['name'][$key];
        $ext = end(explode('.',$filename));
        // Replace all weird characters
        $sanitized = preg_replace('/[^a-zA-Z0-9-_.]/','', substr($filename, 0, -(strlen($ext)+1)));
        // Replace dots inside filename
        $sanitized = str_replace('.','-', $sanitized);
        $nomeImg = strtolower($sanitized.'.'.$ext);

                $file = array(
                    'name' => $nomeImg,
                    'type' => $files['type'][$key],
                    'tmp_name' => $files['tmp_name'][$key],
                    'error' => $files['error'][$key],
                    'size' => $files['size'][$key]
                );

                $_FILES = array ("imagens" => $file);
                foreach ($_FILES as $file => $array) {
          if ($_FILES[$file]['error'] !== UPLOAD_ERR_OK) __return_false();

          require_once(ABSPATH . "wp-admin" . '/includes/image.php');
          require_once(ABSPATH . "wp-admin" . '/includes/file.php');
          require_once(ABSPATH . "wp-admin" . '/includes/media.php');

          $attach_id = media_handle_upload( $file, $id_post );
          if ( is_numeric( $attach_id ) ) {
            $idImage[] = $attach_id;

            // // Pega Patch da imagem
           //    $file = get_attached_file($attach_id);

           //    // carrega o editor cropa e sava a imagem
           //    $image_editor = wp_get_image_editor($file);
           //    $image_editor->resize(785, 615);
           //    $saved = $image_editor->save($file);

           //    // Pega os metadas dados da imagem
           //    $image_meta = get_post_meta($attach_id, '_wp_attachment_metadata', true);

           //    // Atualiza os dados na variavel
           //    $image_meta['height'] = $saved['height'];
           //    $image_meta['width']  = $saved['width'];

           //    // Faz o update
           //    update_post_meta($attach_id, '_wp_attachment_metadata', $image_meta);
          }
                }
            }
    }

  // Insert the post into the database
  // $id_post = wp_insert_post( $my_post );

  // Adiciona Produto
  add_post_meta($id_post, 'produto_utilizado', $_POST['produto-utilizado'] );
  add_post_meta($id_post, '_produto_utilizado', 'field_582357d651db9');

  // Adiciona a aplicação do ambiente
  add_post_meta($id_post, 'ambiente', $_POST['ambiente-categoria'] );
  add_post_meta($id_post, '_ambiente', 'field_58246c7264a9d');

  $relacao = wp_set_object_terms( $id_post, intval($_POST['ambiente-categoria']), 'ambientes' );

  // Adiciona o arquiteto ou projetista
  add_post_meta($id_post, 'projeto', $_POST['titulo'] );
  add_post_meta($id_post, '_projeto', 'field_5824666297ba4');

  // Adiciona o credito da imagem
  add_post_meta($id_post, 'creditos_das_imagens','');
  add_post_meta($id_post, '_creditos_das_imagens', 'field_582466f597ba5');

  // Adiciona o email
  add_post_meta($id_post, 'email',$_POST['email']);
  add_post_meta($id_post, '_email', 'field_5894c6105fbf0');

  // Adiciona o site
  add_post_meta($id_post, 'site',$_POST['site']);
  add_post_meta($id_post, '_site', 'field_58a59d385c80f');

  // Adiciona o telefone
  add_post_meta($id_post, 'telefone',$_POST['telefone']);
  add_post_meta($id_post, '_telefone', 'field_58a59d695c810');

  // Adiciona o observações
  add_post_meta($id_post, 'observacoes',$_POST['observacoes']);
  add_post_meta($id_post, '_observacoes', 'field_58a59d7d5c811');

  $meuArray = serialize ($idImage);
  add_post_meta($id_post, 'galeria',$meuArray);
  add_post_meta($id_post, '_galeria', 'field_58246a4197ba6');

  wp_redirect(site_url().'/envie-seu-projeto?message="sucess"');
}
endif;
add_action( 'admin_post_nopriv_register_ambiente', 'register_form_ambiente' );
add_action( 'admin_post_register_ambiente', 'register_form_ambiente' );


add_action( 'pre_get_posts', 'my_change_sort_order');
    function my_change_sort_order($query){
        if(is_archive()):
         //If you wanted it for the archive of a custom post type use: is_post_type_archive( $post_type )
           //Set the order ASC or DESC
           $query->set( 'order', 'ASC' );
        endif;
    };