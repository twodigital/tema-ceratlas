<?php
if ( ! function_exists( 'form_ambientes_func' ) ) :
function form_ambientes_func() {
  $aplicacao = get_terms(
      array(
        'taxonomy' => 'ambientes',
        'hide_empty' => false,
        'fields' => 'id=>name',
        'orderby' => 'term_order'
      ) );

  $produto = get_posts(
      array(
        'posts_per_page'   => -1,
        'orderby'          => 'title',
        'order'            => 'ASC',
        'post_type'        => 'produtos'
      ) );

  ob_start(); ?>
  <form action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="post" enctype="multipart/form-data" class="contact-form">
    <div class="row">
      <div class="medium-6 columns">
        <input type="text" name="titulo" id="titulo" placeholder="NOME" required>
        <input type="email" name="email" id="email"  placeholder="E-MAIL" required>
        <input type="text" name="site" id="site"  placeholder="SEU SITE OU FANPAGE" required>
        <input type="text" name="telefone" id="telefone"  placeholder="TELEFONE" required>
        <span class="text-label">AMBIENTE PROJETADO:</span>
        <br>
        <div class="ambient-radios">
          <div class="row small-up-2 large-up-3">
            <?php foreach ($aplicacao as $key => $value) {
              $application_image = get_field('icone_ambiente', 'ambientes_' . $key );
              if ($application_image) {
                $img_str = '<img src="' . $application_image['url'] . '" alt="' . $application_image['alt'] . '" class="ambient-image ' . $application_image['title'] . '"/>';
              }
            ?>
            <div class="column column-block">
              <input id="ambient-<?=$key?>" type="radio" name="ambiente-categoria" value="<?=$key?>"/><label for="ambient-<?=$key?>"><?= $img_str ?> <span><?= $value ?></span></label>
            </div>
            <?php } ?>
          </div>
        </div>
      </div>
      <div class="medium-6 columns">
        <select name="produto-utilizado" id="produto-utilizado" required>
          <?php foreach ($produto as $value) { ?>
            <option value="<?= $value->ID?>"><?php echo $value->post_title . " - " . get_field("formato", $value->ID)->name . " (" . get_field("referencia", $value->ID) . ")"; ?></option>
          <?php } ?>
        </select>
        <textarea name="observacoes" id="observacoes" placeholder="OBSERVAÇÕES" rows="8"></textarea>
        <div class="ambient-images">
          <label for="input-images"><i class="fa fa-upload" aria-hidden="true"></i> Envie até <strong>5 imagens</strong></label>
          <input id="input-images" type="file" name="imagens[]" multiple="multiple" required>
          <p>
          Formatos: .JPG ou .PNG<br>
          Dimensões mínimas: 785x615 pixels.<br>
          Tamanho máximo: 2MB.
          </p>
        </div>
        <small>*Imagens sujeitas a aprovação.</small>
        <input type="hidden" name="action" value="register_ambiente">
        <input type="submit" value="ENVIAR">
      </div>
    </div>
  </form>
  <?php return ob_get_clean();
}
endif;

add_shortcode( 'form_ambientes', 'form_ambientes_func' );