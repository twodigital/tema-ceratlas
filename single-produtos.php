<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<?php get_template_part( 'template-parts/featured-image' ); ?>
<div class="row">
  <div class="medium-12 columns">
    <?php foundationpress_breadcrumb(false,true); ?>
  </div>
</div>
<div id="single-products" role="main">
<?php do_action( 'foundationpress_before_content' ); ?>
<?php while ( have_posts() ) : the_post(); ?>
	<article <?php post_class('main-content') ?> id="product-<?php echo str_replace(' ', '-', strtolower(get_field('referencia'))); ?>">
    <h2 class="product-title"><?php the_title(); ?></h2>
    <div class="product-details-container">
      <div class="row">
        <div class="medium-4 columns">
          <?php the_post_thumbnail( 'thumbnail' ); ?>
        </div>
        <div class="medium-8 columns">
          <div class="two-columns">
            <?php if(get_field('referencia')) : ?><strong>Referência:</strong> <?php the_field('referencia') ?><br><?php endif; ?>
            <?php if(get_field('formato')) : ?><strong>Formato:</strong> <?php echo get_field('formato')->name;?><?php endif; ?><?php if(get_field('tipo_de_junta')) : ?> (Junta <?php the_field('tipo_de_junta'); ?>)<br><?php endif; ?>
            <?php
            if(get_field('cor')) :
              foreach (get_field('cor') as $cor) :
                $color_names[] = $cor->name;
              endforeach;

            ?>
                <strong><?php if (count($color_names) > 1) { echo "Cores:"; } else { echo "Cor:"; } ?></strong>  <?php echo implode(', ', $color_names) ?><br>
            <?php

            endif; ?>
            <?php if(get_field('variacao_de_tonalidade')) : ?><strong>Variação de tonalidade:</strong> <?php the_field('variacao_de_tonalidade'); ?><br><?php endif; ?>
            <?php if(get_field('disponibilidade')) : ?><strong>Disponibilidade:</strong> <?php echo get_field('disponibilidade')->name; ?><br><?php endif; ?>
            <?php if(get_field('caracteristicas')) : ?><strong>Características:</strong> <?php echo get_field('caracteristicas'); ?><br><?php endif; ?>
            <?php if(get_field('espessura')) : ?><strong>Espessura:</strong> <?php echo get_field('espessura'); ?>mm<br><?php endif; ?>
            <?php if(get_field('m2_por_caixa')) : ?><strong>m² por caixa:</strong> <?php echo get_field('m2_por_caixa'); ?>m²<br><?php endif; ?>
            <?php if(get_field('m2_por_pallet')) : ?><strong>m² por pallet:</strong> <?php echo get_field('m2_por_pallet'); ?>m²<br><?php endif; ?>
            <?php if(get_field('caixas_por_pallet')) : ?><strong>Caixas por pallet:</strong> <?php echo get_field('caixas_por_pallet'); ?>cxs<br><?php endif; ?>
            <?php if(get_field('peso_por_caixa')) : ?><strong>Peso por caixa:</strong> <?php echo get_field('peso_por_caixa'); ?>kg<br><?php endif; ?>
            <?php if(get_field('peso_por_pallet')) : ?><strong>Peso por pallet:</strong> <?php echo get_field('peso_por_pallet'); ?>kg<br><?php endif; ?>
            <?php if(get_field('junta_interna_da_placa')) : ?><strong>Junta interna da placa:</strong> <?php echo get_field('junta_interna_da_placa'); ?>mm<br><?php endif; ?>
            <!--<?php if(get_field('armazenamento')) : ?><strong>Armazenamento:</strong> <?php echo get_field('armazenamento'); ?><br><?php endif; ?>-->
            <?php if(get_field('pei')) : ?><strong>PEI:</strong> <?php echo get_field('pei'); ?><br><?php endif; ?>
            <?php if(get_field('indicacao_de_uso')) : ?><strong>Indicação de uso:</strong> <?php echo get_field('indicacao_de_uso'); ?><br><?php endif; ?>
            <?php if(get_field('tamanho_real')) : ?><strong>Tamanho Real:</strong> <?php echo get_field('tamanho_real'); ?>cm<br><?php endif; ?>
            <?php if(get_field('dimensao_da_placa')) : ?><strong>Dimensão da placa:</strong> <?php echo get_field('dimensao_da_placa'); ?><br><?php endif; ?>
            <?php if(get_field('area_por_m2_da_placa')) : ?><strong>Área por m² da placa:</strong> <?php echo get_field('area_por_m2_da_placa'); ?><br><?php endif; ?>
            <?php if(get_field('pecas_por_placa')) : ?><strong>Peças por placa:</strong> <?php echo get_field('pecas_por_placa'); ?><br><?php endif; ?>
            <?php if(get_field('placas_por_m2')) : ?><strong>Placas por m²:</strong> <?php echo get_field('placas_por_m2'); ?><br><?php endif; ?>
            <?php if(get_field('placas_por_caixa')) : ?><strong>Placas por caixa:</strong> <?php echo get_field('placas_por_caixa'); ?><br><?php endif; ?>
          </div>
          <div class="text-right">
            <a class="hollow button secondary" href="/manuais-tecnicos/">Manuais técnicos</a>
            <a class="hollow button secondary" href="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ) ?>">Baixar imagem</a>
            <a class="hollow button secondary" href="/onde-comprar/">Onde comprar</a>
          </div>
        </div>
      </div>


      <div class="applications-container">
        <span class="applications-label">ALGUNS AMBIENTES QUE APLICAM ESTE PRODUTO:</span>
        <div class="applications-row">
          <div class="applications-columns-6">
            <?php
              $postid = $post->ID;
              $product_ambients_ids = $product_application_images = $product_application_ids= array();
              $args = array(
                'post_type' => 'aplicacoes'
              );
              $loop = new WP_Query( $args );
              while ( $loop->have_posts() ) : $loop->the_post();
                if (!in_array($postid, get_field('produto_utilizado'))) {
                    continue;
                }
                $product_application_ids[] = $post->ID;
                $product_application_images[] = get_field('galeria');
                $product_ambients_ids[] = get_field('ambiente')->term_id;
              endwhile;

              $applications = get_terms( array('taxonomy' => 'ambientes','hide_empty' => false, 'fields' => 'id=>name', 'orderby' => 'id') );
              if ($applications) {
                echo '<div class="small-up-2 medium-up-2 large-up-6 application-list">';
                foreach ($applications as $application_id => $application) {
                  $application_used_class = in_array($application_id, $product_ambients_ids) ? 'used' : NULL;
                  echo '<div class="column column-block">';
                    echo '<div class="application ' . $application_used_class . '">';
                      $application_image = get_field('icone_ambiente', 'ambientes_' . $application_id );
                      if ($application_image) {
                        echo '<img src="' . $application_image['url'] . '" alt="' . $application_image['alt'] . '" class="' . $application_image['title'] . '"/>';
                      }
                      echo '<span>' . $application . '</span>';
                    echo '</div>';
                  echo '</div>';
                }
                echo '</div>';
              }
            ?>
          </div>
          <div class="applications-columns-1">
            <div style="margin: 20px;"><a class="hollow button secondary expanded button-more" href="/aplicacoes-e-ambientes/"><i class="fa fa-plus-circle" aria-hidden="true"></i> VER TODOS AMBIENTES</a></div>
          </div>
        </div>
      </div>

    </div>
    <div class="row">
      <div class="medium-12 columns">
        <?php
        if ($product_application_images) :
          echo '<div id="slide-applications">';
            echo '<h2><strong>CONHEÇA OS AMBIENTES</strong></h2>';
              echo '<div class="bx-slider">';
                foreach ($product_application_images as $key => $images) {
                  foreach($images as $image) {
                    echo '<a class="link-application" href="' . $image['sizes']['application-gallery'] . '" data-projeto="' . get_field("projeto", $product_application_ids[$key]) .'" data-creditos="' . get_field("creditos_das_imagens", $product_application_ids[$key]) .'">';
                      echo  '<img src="' . $image['sizes']['thumbnail'] . '" >';
                    echo '</a>';
                  }
                }
              echo '</div>';
          echo '</div>';
        endif;
        ?>
      </div>
    </div>
    <div class="send-your-project">
      <div class="row" data-equalizer data-equalize-on="medium">
        <div class="large-7 columns" data-equalizer-watch>
        <p class="project-teaser"><strong>Arquitetos, Designers e Engenheiros,</strong><wbr> enviem seus projetos para Cerâmica Atlas.</p>
        <hr>
        <p class="project-text">Esta área é dedicada para o envio de projetos com aplicações de nossos produtos. Iremos divulgá-los em nossa vitrine.</p>
        </div>
        <div class="large-4 columns medium-offset-1" data-equalizer-watch>
          <a class="hollow button large secondary expanded button-more" href="/envie-seu-projeto/">SAIBA COMO ENVIAR</a>
        </div>
      </div>
    </div>
    <?php
      wp_reset_postdata();
      $args = array(
        'post_type'       => 'produtos',
        'post__not_in'    => array($postid),
        'orderby'         => 'rand',
        'posts_per_page'  => 5,
        'tax_query'       => array(
            array(
              'taxonomy'  => 'formats',
              'field'     => 'term_id',
              'terms'     => get_field('formato')->term_id,
            ),
          ),
      );
      $loop = new WP_Query( $args );
      if ($loop->have_posts()) :
        echo '<div class="related-products">';
          echo '<h2><strong>OUTROS PRODUTOS RELACIONADOS</strong></h2>';
          echo '<div class="row small-up-2 medium-up-4 large-up-5">';
          while ( $loop->have_posts() ) : $loop->the_post();
              echo '<div class="column column-block">';
                echo '<a href="' . get_permalink( $post->ID ) . '">';
                  the_post_thumbnail( 'thumbnail' );
                  echo '<span class="related-product-title">' . get_the_title() . '</span>';
                echo '</a>';
              echo '</div>';
          endwhile;
          echo '</div>';
        echo '</div>';
      endif;
    ?>
	</article>
<?php endwhile; ?>

<?php do_action( 'foundationpress_after_content' ); ?>
<?php get_sidebar(); ?>
</div>
<?php get_footer();
