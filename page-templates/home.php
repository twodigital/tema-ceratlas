<?php
/*
Template Name: Home
*/
get_header(); ?>

<div id="page-home">
  <?php
   $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
   $images = get_field('slider_images', $term);
   $images_total = count($images);
   if ($images) {
    if ($images_total > 1) {
      echo '<div class="slider-home slide-on">';
    } else {
      echo '<div class="slider-home">';
    }
    foreach( $images as $image ) {
      echo '<div class="slide">';
        echo '<img src="' . $image['url'] . '" alt="' . $image['alt'] . '" />';
        echo '<div class="slide-description">';
          echo '<span>' . $image['description'] . '</span>';
        echo '</div>';
      echo '</div>';
    }
    echo '</div>';
   }

  ?>
  <?php get_template_part( 'template-parts/search' ); ?>

    <div class="home-content">
    <!--
      <div class="row">
        <div class="medium-12 columns">
          <p class="products-teaser"><strong>Toda a beleza dos tradicionais produtos da Cerâmica Atlas</strong> em mais de 90 tipos diferentes de pastilhas</p>
        </div>
      </div>
    -->

    <div class="row">
      <div class="medium-9 small-centered" style="overflow: hidden;">
        <div class="row">
          <div class="medium-6 columns"><a href="/produtos/disponibilidade/pronta-entrega/" class="link-products link-imediate-delivery"><span class="v-a"><span class="link-title">REVENDAS</span><span class="link-description">PRODUTOS DISPONÍVEIS, A PRONTA ENTREGA, PARA LOJAS AUTORIZADAS</span></span></a></div>
          <div class="medium-6 columns"><a href="/produtos/disponibilidade/sob-encomenda/" class="link-products link-custom-made"><span class="v-a"><span class="link-title">ENGENHARIA</span><span class="link-description">PRODUTOS SOB ENCOMENDA, PARA ATENDIMENTO AS CONSTRUTORAS, CONDOMÍNOS E GRANDES OBRAS</span></span></a></div>
        </div>
      </div>
    </div>



    <div class="projetos-atlas">
      <div class="row">
        <div class="medium-12 columns">
          <a class="link-magazine" href="/aplicacoes/">PROJETOS COM ATLAS&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<strong>INSPIRE-SE</strong></a>
        </div>
      </div>
    </div>


    <div class="row">
      <div class="medium-5 columns">
          <div class="instagram-feed">
            <p class="instagram-text"><strong>Acompanhe a gente no</strong> INSTAGRAM <a class="instagram-link" href="https://www.instagram.com/ceramicaatlas/">@CERAMICAATLAS</a></p>
            <?php echo do_shortcode("[instagram-feed]"); ?>
            <p><a href="https://www.facebook.com/ceratlas" style="color: #3b5998"><i class="fa fa-facebook-official" aria-hidden="true"></i></a> <small>#CERAMICAATLAS</small></p>
          </div>
      </div>
      <div class="medium-6 large-offset-1 columns">
        <div class="send-your-project">
          <p class="project-teaser"><strong>Arquitetos, Designers e Engenheiros,</strong> enviem seus projetos para Cerâmica Atlas.</p>
          <hr>
          <p class="project-text">Esta área é dedicada para o envio de projetos com aplicações de nossos produtos. Iremos divulgá-los em nossa vitrine.</p>
          <br>
          <a href="/envie-seu-projeto/" class="large hollow secondary button">SAIBA COMO ENVIAR</a>
        </div>

      </div>
    </div>
  </div>
  <?php get_footer(); ?>
</div>