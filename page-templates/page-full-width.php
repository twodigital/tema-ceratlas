<?php
/*
Template Name: Full Width
*/
get_header(); ?>

<?php get_template_part( 'template-parts/featured-image' ); ?>
<div class="row">
  <div class="medium-12 columns">
    <?php foundationpress_breadcrumb(false,true); ?>
  </div>
</div>

<div id="page-full-width" role="main">
  <?php do_action( 'foundationpress_before_content' ); ?>
  <?php while ( have_posts() ) : the_post(); ?>
  <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
    <?php if ( !has_post_thumbnail( $post->ID ) ) : ?>
      <header>
        <h1 class="entry-title"><?php echo get_the_title($post->ID); ?></h1>
        <p><?php echo get_the_subtitle( $post->ID); ?></p>
      </header>
    <?php endif; ?>
    <?php do_action( 'foundationpress_page_before_entry_content' ); ?>
    <div class="entry-content">
        <?php the_content(); ?>
    </div>
  </article>
<?php endwhile;?>

<?php do_action( 'foundationpress_after_content' ); ?>

</div>

<?php get_footer();

