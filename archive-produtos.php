<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */
  get_header(); ?>

  <?php get_template_part( 'template-parts/featured-image' ); ?>

  <?php $verificaUrl = strripos($_SERVER['REQUEST_URI'],'pronta-entrega'); ?>

  <div class="row">
    <div class="medium-12 columns">
      <?php foundationpress_breadcrumb(false,true); ?>
    </div>
  </div>
  <div id="page-products" role="main">
    <article class="main-content">
      <div class="row">

        <div class="medium-12 columns">
          <button class="formats-button hollow secondary dropdown button" data-toggle="example-dropdown">Formatos</button>
          <div class="formats-pane" id="example-dropdown" data-dropdown data-auto-focus="true" data-pronta="<?php echo ($verificaUrl == false) ? 'false':'true' ?>">
          <?php dynamic_sidebar( 'products-widgets' ); ?>
          </div>
        </div>
      </div>

      <?php if ( have_posts() ) : ?>

        <?php /* Start the Loop */ ?>
        <div class="row small-up-1 medium-up-3 large-up-5 products-list">
          <?php while ( have_posts() ) : the_post(); ?>
            <div class="column column-block product-container">
             <?php get_template_part( 'template-parts/content', "produtos" ); ?>
            </div>
          <?php endwhile; ?>
        </div>
        <?php else : ?>
          <?php get_template_part( 'template-parts/content', 'none' ); ?>

        <?php endif; // End have_posts() check. ?>
        <?php /* Display navigation to next/previous pages when applicable */ ?>
        <?php
        if ( function_exists( 'foundationpress_pagination' ) ) :
          foundationpress_pagination();
        elseif ( is_paged() ) :
        ?>
          <nav id="post-nav">
            <div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'foundationpress' ) ); ?></div>
            <div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'foundationpress' ) ); ?></div>
          </nav>
        <?php endif; ?>
    </article>
  <?php get_sidebar(); ?>

</div>

<?php get_footer();
