$(function() {
  if ($("#boleto").length) {
    $('input[name="data-vencimento"]').mask('99/99/9999');

    var input_identity = $('input[name="numero-identidade"]');
    input_identity.unmask().mask("99.999.999/9999-99");
    $('input[name="cnpj-cpf"]').on("change", function() {
      switch ($(this).val()) {
        case 'CNPJ':
        input_identity.unmask().mask("99.999.999/9999-99");
           break;
        case 'CPF':
        input_identity.unmask().mask("999.999.99-99");
           break;
      }
    });
  }
});