$(function() {
  if ($(".search-bar").length) {

    $("#search-form")[0].reset();

    $(".search-toggle").on("click", function(e) {
      e.preventDefault();
      $(".search-content").toggleClass("hide");
      $(".search-carets .fa").toggleClass("fa-angle-down fa-angle-up");
    });

    $(".search-disponibilidade input[type='radio']").on("change", function() {
      $(".search-taxonomies-selector, .search-button-bar").removeClass("hide");
    });

    $("#a-pronta-entrega").on("click", function(e) {
      $("label[for='10-x-10-cm']").hide();
    });

    $("#sob-encomenda").on("click", function(e) {
      $("label[for='10-x-10-cm']").show();
    });

    $(".search-taxonomies-selector input[type='radio']").on("change", function() {
      switch ($(this).val()) {
        case 'c':
          $(".search-colors, .search-formats, .search-ambients").addClass("hide");
          $(".search-colors").removeClass("hide");
          break;

        case 'f':
          $(".search-colors, .search-formats, .search-ambients").addClass("hide");
          $(".search-formats").removeClass("hide");
          break;

        case 'a':
          $(".search-colors, .search-formats, .search-ambients").addClass("hide");
          $(".search-ambients").removeClass("hide");
          break;
      }
    });

    $('.search-colors input[type="checkbox"]').on('change', function() {
       if($(this).siblings(':checked').length >= 3) {
           this.checked = false;
       }
    });
  }
});