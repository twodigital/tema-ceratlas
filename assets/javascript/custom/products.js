$(function() {

  if ($("#slide-applications").length) {
    $("#slide-applications .bx-slider").bxSlider({ minSlides: 5, maxSlides: 10, slideWidth: 180, slideMargin: 20, slideMove: 0, controls: true, pager: false });

    $(".link-application").on("click", function (e) {
        e.preventDefault();
        var el = $(this);
        createApplicationsModal(el);
      });
  }

  var prontaentrega = $('.formats-pane').data('pronta');
    if(prontaentrega === true){
      $('.cat-item-31').hide();
    }
});
function getApplicationIndex(element) {
    var elementList = Array.prototype.slice.call(element.parents(".bx-slider").children().not(".bx-clone"));
    return elementList.indexOf(element[0]);
}

function createApplicationsModal(element) {
    if (!element.length) {
      return;
    }
    var modal = $("<div class=\"reveal gallery-modal\"><button class=\"close-button\" data-close aria-label=\"Fechar galeria\" type=\"button\"><span aria-hidden=\"true\">&times;</span></button></div>"),
        application_gallery = createApplicationGallery();

    application_gallery.appendTo(modal);

    modal.on("closed.zf.reveal", function() { $(this).remove(); });
    modal.on("open.zf.reveal", function() {
      var slide_index = getApplicationIndex(element);
     $(this).children("#application-gallery").bxSlider({ pager:false, adaptiveHeight: false, auto: true, mode: 'fade', speed: 1000, pause: 5000, startSlide: slide_index });
    });

    var fndReveal = new Foundation.Reveal(modal);
    fndReveal.open();
}

function createApplicationGallery() {
  var application_gallery = $("<div id=\"application-gallery\"></div>");
  $(".link-application:not(.bx-clone)").each(function() {
    var el = $(this),
        image_string = "<img src=\"" + el.attr("href") + "\" />",
        project_string = (el.data("projeto").length) ?  "<strong>Projetista:</strong> <span>" + el.data("projeto") + "</span>" : "",
        copyright_string = (el.data("creditos").length) ? "<strong>Créditos:</strong> <span>" + el.data("creditos") + "</div>" : "";

        if (image_string.length){
          var gallery_item = $("<div class=\"gallery-item\">" + image_string + "<div class=\"gallery-item-info\">" + project_string + "<div class=\"float-right\">" + copyright_string + "</div></div>");
          gallery_item.appendTo(application_gallery);
        }

  });
  return application_gallery;
}