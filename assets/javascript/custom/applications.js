$(function() {
  if ($("#page-applications").length) {
    $(".link-application").on("click", function (e) {
      e.preventDefault();
      var el = $(this);
      createGalleryModal(el);
    });
  }
});

function createGalleryModal(element) {
  if (element.length) {
    var modal = $("<div class=\"reveal gallery-modal\"><button class=\"close-button\" data-close aria-label=\"Fechar galeria\" type=\"button\"><span aria-hidden=\"true\">&times;</span></button></div>"),
        gallery = createGallery(element),
        thumbs = createGalleryThumbs(element),
        infos = getApplicationInfos(element);

    gallery.appendTo(modal);
    thumbs.appendTo(modal);
    infos.appendTo(modal);


    modal.on("closed.zf.reveal", function() { $(this).remove(); });
    modal.on("open.zf.reveal", function() {
      $(this).children("#bxslider").bxSlider({ pagerCustom: '#bx-pager', adaptiveHeight: false, auto: true, mode: 'fade', speed: 1000, pause: 5000 });
      $(this).children("#bx-pager").bxSlider({ minSlides: 5, maxSlides: 10, slideWidth: 100, slideMargin: 10, slideMove: 0 });
    });

    var fndReveal = new Foundation.Reveal(modal);
    fndReveal.open();
  }
}

function createGallery(element) {
  var arr_images = element.children(".img-application").data("images").split(','),
      gallery = $("<ul id=\"bxslider\"></ul>"),
      gallery_item;

  if (arr_images.length) {
    arr_images.forEach(function(img_url) {
      gallery_item = $("<li><img src=\"" + img_url + "\" /></li>");
      gallery_item.appendTo(gallery);
    });
  }
  return gallery;
}

function createGalleryThumbs(element) {
  var arr_images = element.children(".img-application").data("images-thumbs").split(','),
      thumbs = $("<div id=\"bx-pager\"></div>"),
      thumbs_item;

  if (arr_images.length) {
    arr_images.forEach(function(img_url, index) {
      thumbs_item = $("<a data-slide-index=\"" + index + "\" href=\"\"><img src=\"" + img_url + "\" /></a>");
      thumbs_item.appendTo(thumbs);
    });
  }
  return thumbs;
}

function getApplicationInfos(element) {
  var application_infos = element.children(".application-infos").clone().removeClass("hide");
  return application_infos;
}