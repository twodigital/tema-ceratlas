<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

    </section>

    <div id="footer-container">
      <footer id="footer">
        <?php do_action( 'foundationpress_before_footer' ); ?>
        <?php //dynamic_sidebar( 'footer-widgets' ); ?>
        <div class="row">
          <div class="medium-5 columns">
            <div class="subscribe">
              <p class="subscribe-text"><strong>Cadastre e receba novidades e lançamentos dos nossos produtos</strong></p>
              <?php echo do_shortcode('[rainmaker_form id="144"]') ?>
            </div>
          </div>
          <div class="medium-6 medium-offset-1 columns">
            <div class="footer-links">
              <a href="http://sistema.ceramicaatlas.com.br/ceramicaatlas/Novo/cadSistema.php"><i class="fa fa-unlock-alt" aria-hidden="true"></i> Área de login</a>
              <a href="/boleto/"><i class="fa fa-list-alt" aria-hidden="true"></i> Boleto</a>
              <a href="http://sistema.ceratlas.com.br/ceramicaatlas/Portugues/sistema_comissao/"><i class="fa fa-handshake-o" aria-hidden="true"></i> Comissões</a>
            </div>
            <hr>
            <address>
              <i class="fa fa-phone" aria-hidden="true"></i> <a class="link-phone" href="tel:+551936739600">+55 (19) 3673-9600</a><br class="show-for-small-only">
              <i class="fa fa-envelope" aria-hidden="true"></i> <a class="link-email" href="mailto:faleconosco@ceratlas.com.br">faleconosco@ceratlas.com.br</a><br>
              <span class="address-text">Rodovia Padre Donizetti, SP 332, Km 272 - Tambaú/SP - <br class="show-for-small-only"> CEP: 13.710-000</span><br>
              <a href="https://www.instagram.com/ceramicaatlas/" title="Instagram Cerâmica Atlas"><i class="fa fa-instagram" aria-hidden="true"></i></a> <a href="https://www.facebook.com/ceratlas" title="Facebook Cerâmica Atlas"><i class="fa fa-facebook-square" aria-hidden="true"></i></a> <span class="social-text">Nossas redes sociais</span>
            </address>
          </div>
        </div>
        <?php do_action( 'foundationpress_after_footer' ); ?>
      </footer>
    </div>

    <div class="two-bar">
      <div class="row">
        <div class="medium-12 columns">
          projeto: <a href="http://www.twodigital.com.br"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/identity/logo-two-digital.png" alt="Two Digital"></a>
        </div>
      </div>
    </div>


    <?php do_action( 'foundationpress_layout_end' ); ?>

<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) === 'offcanvas' ) : ?>
    </div><!-- Close off-canvas wrapper inner -->
  </div><!-- Close off-canvas wrapper -->
</div><!-- Close off-canvas content wrapper -->
<?php endif; ?>


<?php wp_footer(); ?>
<?php do_action( 'foundationpress_before_closing_body' ); ?>
<script type="text/javascript">
var _tn = _tn || [];
_tn.push(['account','643cb831e5b06969c34a7d4b7add4943']);
_tn.push(['action','track-view']);
(function() {
document.write(unescape("<span id='tolvnow'></span>"));
var tss = document.createElement('script'); tss.type = 'text/javascript'; tss.async = true;
tss.src = '//tracker.tolvnow.com/js/tn.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tss, s);
})();
</script>
</body>
</html>
