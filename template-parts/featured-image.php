<?php
// If a featured image is set, insert into layout and use Interchange
// to select the optimal image size per named media query.

  $featured_image_id = $title = $subtitle = $classes = NULL;
  $small_header = get_field('small_header');

  if (is_category()) {
    $term = get_term_by( 'slug', get_query_var( 'category_name' ), 'category');
  }
  else {
    $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
  }

  if ($term) {
    $featured_image_id = get_field('header_image', $term);
    $title             = $term->name;
    $subtitle          = $term->description;
  }
  elseif ( has_post_thumbnail( $post->ID ) ) {
    $featured_image_id =  get_post_thumbnail_id();
    if (is_single()) {
      $small_header = true;
    } else {
      $title             = get_the_title($post->ID);
      $subtitle          = get_the_subtitle( $post->ID,"","",false);
    }
  }

  if ($featured_image_id) {
    $featured_small   = wp_get_attachment_image_src($featured_image_id, "featured-small" );
    $featured_medium  = wp_get_attachment_image_src($featured_image_id, "featured-medium" );
    $featured_large   = wp_get_attachment_image_src($featured_image_id, "featured-large" );
    $featured_xlarge  = wp_get_attachment_image_src($featured_image_id, "featured-xlarge" );

    $data_interchange = "data-interchange=\"[" . $featured_small[0] . ", small], [" . $featured_medium[0] . ", medium], [" . $featured_large[0] . ", large], [" . $featured_xlarge[0] .", xlarge]\"";
  }

  $classes = $small_header ? "class=\"small-header\"" : "";

  if ( has_post_thumbnail( $post->ID ) || $term || is_category() ) : ?>
    <header id="featured-hero" role="banner" <?php echo $data_interchange;  echo $classes; ?>>
      <div class="featured-title">
        <?php if ($title) : ?>
          <h1><?php echo $title; ?></h1>
        <?php endif; ?>
        <?php if ($subtitle) : ?>
          <p><?php echo $subtitle; ?></p>
        <?php endif; ?>
      </div>
    </header>
    <?php get_template_part( 'template-parts/search' ); ?>
<?php endif;