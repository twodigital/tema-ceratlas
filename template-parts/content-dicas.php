<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */
?>

<?php
  $post_index = $wp_query->current_post + 1;
  $count_posts = wp_count_posts();
  $column_block_class = "";
  $bg_string = "";

  if (($post_index == 1) || ($post_index == 3) ) {
    $column_block_class = "double";
  }

  if ($post_index == 1) : ?>
  <div class="column column-block item-dicas white">
    <div class="va-m">
      <h2><strong>SEPARAMOS ALGUMAS DICAS PARA TE AUXILIAR EM CADA ETAPA DA OBRA</strong></h2>
    </div>
  </div>
<?php endif;

  if ($post_index == 4) : ?>
  <div class="column column-block item-dicas white">
    <div class="va-m">
      <p>A <strong>Cerâmica Atlas</strong> tem a preocupação de manter a excelência em todas as etapas da construção. </p>
      <p>Vai construir, reformar? Consulte-nos e fale com a gente através do chat.</p>
    </div>
  </div>
<?php endif;

  if ($post_index == 5) : ?>
    <div class="column column-block item-dicas white">
      <div class="va-m">
        <p>Disponibilizamos recursos para você não ficar em dúvida na hora de aplicar nossos produtos.</p>
        <p>Consulte nossos <a href="/manuais-tecnicos/">manuais técnicos</a>.</p>
      </div>
    </div>
  <?php endif;

  if ( has_post_thumbnail($post->ID) ) {
    $bg_string = 'style="background-image: url(' . get_the_post_thumbnail_url($post->ID, 'full') . ');"';
  }
  ?>



  <div class="column column-block item-dicas <?= $column_block_class; ?>">
    <a href="<?php the_permalink(); ?>" <?= $bg_string ?>>
      <div class="va-m">

        <span class="dicas-title"><?php echo get_the_title(); ?></span>
        <span class="dicas-subtitle"><?php echo get_the_subtitle( $post->ID,"","",false); ?></span>
      </div>
    </a>
  </div>
