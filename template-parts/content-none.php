<?php
/**
 * The template part for displaying a message that posts cannot be found
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

<header class="page-header">
	<h3 class="page-title">Nada foi encontrado! :(</h3>
</header>

<div class="page-content">
	<p>Tente a pesquisar novamente.</p>
</div>
