<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

<div id="application-<?php echo str_replace(' ', '-', strtolower(get_field('referencia'))); ?>" class="application" >
    <a href="#" class="link-application">
      <?php
        $images = get_field('galeria');
        $image_arr = $images_thumbs_arr= array();
        if( $images ) {
          foreach ($images as $image) {
            $image_arr[] = $image['sizes']['application-gallery'];
            $images_thumbs_arr[] = $image['sizes']['thumbnail'];
          }
          $image_data = implode(',', $image_arr);

          $images_thumbs_data = implode(',', $images_thumbs_arr);

          echo  '<img class="img-application" src="' . $images[0]['sizes']['thumbnail'] . '" data-images="' . $image_data . '" data-images-thumbs="' . $images_thumbs_data . '" ><br>';
        }

        $products_used = get_field('produto_utilizado');
        foreach ($products_used as $product) {
          $product_post = get_post($product);
          echo '<i class="fa fa-th-large" aria-hidden="true"></i> ' . get_the_title($product_post) . ' (' . get_field("formato", $product_post)->name . ')<br>';
          echo '<i class="fa fa-hashtag" aria-hidden="true"></i> ' . get_field("referencia", $product_post) . '<br>';
          echo '<i class="fa fa-user" aria-hidden="true"></i> ' . get_field("projeto") . '<br>';
          echo '<i class="fa fa-copyright" aria-hidden="true"></i> ' . get_field("creditos_das_imagens");

          echo '<div class="application-infos hide">';
            echo '<div class="row">';
              echo '<div class="small-3 columns">';
                   echo get_the_post_thumbnail($product_post->ID, "thumbnail");
              echo '</div>';
              echo '<div class="small-9 columns">';
                echo '<div class="infos">';
                  echo '<strong>PASTILHA:</strong> '. get_the_title($product_post) . ' (' . get_field("formato", $product_post)->name . ')<br>';
                  echo '<strong>REFERÊNCIA:</strong> ' . get_field("referencia", $product_post) . '<br>';
                  echo '<strong>PROJETO:</strong> ' . get_field("projeto") . '<br>';
                  echo '<strong>CRÉDITOS:</strong> ' . get_field("creditos_das_imagens");
                echo '</div>';
              echo "</div>";
            echo '</div>';
          echo '</div>';
        }
      ?>
    </a>
</div>
