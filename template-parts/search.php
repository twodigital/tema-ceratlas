<?php
  $cores = get_terms(
      array(
        'taxonomy' => 'colors',
        'hide_empty' => false,
        'fields' => 'id=>name',
        'orderby' => 'term_order',
      ) );

  $tamanhos = get_terms(
      array(
        'taxonomy' => 'formats',
        'hide_empty' => false,
        'fields' => 'id=>name',
        'orderby' => 'term_order'
      ) );

  $disponibilidade = get_terms(
      array(
        'taxonomy' => 'disponibilidade',
        'hide_empty' => false,
        'fields' => 'id=>name',
        'orderby' => 'term_order'
      ) );

  $aplicacao = get_terms(
      array(
        'taxonomy' => 'ambientes',
        'hide_empty' => false,
        'fields' => 'id=>name',
        'orderby' => 'term_order'
      ) );
?>

<div class="search-bar">
  <div class="search-container">
    <a href="#" class="search-toggle">
      <i class="fa fa-search" aria-hidden="true"></i> <span class="placeholder-text"><span class="hide-for-small-only">SELECIONE AQUI SUA</span> BUSCA PERSONALIZADA</span> <span class="search-carets"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
    </a>
    <div class="search-content hide">
      <form id="search-form" action="<?= esc_url(home_url())?>" method="GET" role="">

        <input type="hidden" name="s" value="<?= get_search_query() ?>" />
        <input type="hidden" name="post_type" value="produtos" />

        <?php if ($disponibilidade) : ?>
          <div class="row">
            <div class="medium-6 columns">
              <div class="search-disponibilidade">
              <?php  foreach ($disponibilidade as $key => $value) : ?>
                <input type="radio" id="<?= str_replace(' ', '-', strtolower($value)) ?>" name="disponibilidade" value="<?= $key ?>"><label for="<?= str_replace(' ', '-', strtolower($value)) ?>"> <?= $value ?></label>
              <?php endforeach; ?>
              </div>
            </div>
            <div class="medium-6 columns">
              <div class="search-taxonomies-selector hide">
                <input type="radio" name="t" id="color-selector" value="c"><label for="color-selector">Cores</label>
                <input type="radio" name="t" id="format-selector" value="f"><label for="format-selector">Formatos</label>
                <!--<input type="radio" name="t" id="ambient-selector" value="a"><label for="ambient-selector">Ambientes</label>-->
              </div>
            </div>
          </div>
          <div class="search-colors hide">
            <div class="row">
              <div class="medium-12 columns">
                <p>ESCOLHA ATÉ 3 CORES:</p>
                <?php foreach ($cores as $color_id => $color_name) : ?>
                  <input type="checkbox" id="<?= str_replace(' ', '-', strtolower($color_name)) ?>" name="color[]" value="<?= $color_id ?>"><label for="<?= str_replace(' ', '-', strtolower($color_name)) ?>"><span class="color-square" style="background-color: <?= get_field("hexa_cor", "colors_" . $color_id); ?>"></span> <span class="color-name"><?= $color_name ?></span></label>
                <?php endforeach; ?>
              </div>
            </div>
          </div>

          <div class="search-formats hide">
            <div class="row">
              <div class="medium-12 columns">
                <p>ESCOLHA UM FORMATO:</p>
                <?php foreach ($tamanhos as $key => $value) { ?>
                  <input type="radio" id="<?= str_replace(' ', '-', strtolower($value)) ?>" name="tamanho" value="<?= $key ?>"><label for="<?= str_replace(' ', '-', strtolower($value)) ?>"> <?= $value ?></label>
                <?php } ?>
              </div>
            </div>
          </div>
<!--
          <div class="search-ambients hide">
            <p>ESCOLHA UM AMBIENTE:</p>
            <?php foreach ($aplicacao as $key => $value) :
              $application_image = get_field('icone_ambiente', 'ambientes_' . $key );
              if ($application_image) {
                $img_str = '<img src="' . $application_image['url'] . '" alt="' . $application_image['alt'] . '" class="ambient-image ' . $application_image['title'] . '"/>';
              }
            ?>
              <input type="radio" id="<?= str_replace(' ', '-', strtolower($value)) ?>" name="aplicacao" value="<?= $key ?>"><label for="<?= str_replace(' ', '-', strtolower($value)) ?>"> <?= $img_str ?> <span class="ambient-title"><?= $value ?></span></label>
            <?php endforeach; ?>
          </div>
-->
          <div class="search-button-bar hide">
            <input type="submit" value="Pesquisar" class="button" />
          </div>
        <?php endif; ?>
      </form>
    </div>
  </div>
</div>