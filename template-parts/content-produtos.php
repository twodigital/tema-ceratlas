<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

<div id="product-<?php echo str_replace(' ', '-', strtolower(get_field('referencia'))); ?>" class="product" >
    <a href="<?php the_permalink(); ?>" class="link-product">
      <?php the_post_thumbnail( 'thumbnail' ); ?>
      <span class="product-title"><?php the_title(); ?></span>
    </a>
</div>

