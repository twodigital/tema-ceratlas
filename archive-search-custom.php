<?php get_header();

$metaArray = array();
$metaArray['relation'] = 'AND';

if ($_GET['color'] != null || !empty($_GET['color'])) {
  $metaArray[]['relation'] = 'OR';
  foreach ($_GET['color'] as $key => $value) {
    $corSearch = intval($value);
    $metaArray[0][] = array(
                      'key' => 'cor',
                      'value' => $corSearch,
                      'compare' => 'LIKE'
                  );
  }
}

if ($_GET['disponibilidade'] != null || !empty($_GET['disponibilidade'])) {
  $disponibilidadeSearch = intval($_GET['disponibilidade']);
  $metaArray[] =  array(
                      'key' => 'disponibilidade',
                      'value' => $disponibilidadeSearch,
                      'compare' => 'LIKE'
                  );
}

if ($_GET['tamanho'] != null || !empty($_GET['tamanho'])) {
  $tamanhoSearch = intval($_GET['tamanho']);
  $metaArray[] =  array(
                      'key' => 'formato',
                      'value' => $tamanhoSearch,
                      'compare' => 'LIKE'
                  );
}

if ($_GET['aplicacao'] != null || !empty($_GET['aplicacao'])) {
  $aplicacaoSearch = intval($_GET['aplicacao']);
  $metaArray[] =  array(
                      'key' => 'aplicacao',
                      'value' => $aplicacaoSearch,
                      'compare' => 'LIKE'
                  );
}

if ($_GET['s'] != null || !empty($_GET['s'])) {
  $q1 = new WP_Query( array(
      'post_type' => 'produtos',
      'posts_per_page' => -1,
      's' => $_GET['s']
  ));

  $q2 = new WP_Query( array(
      'post_type' => 'produtos',
      'posts_per_page' => -1,
      'meta_query' => array(
          array(
            'key' => 'referencia',
            'value' => $_GET['s'],
            'compare' => 'LIKE'
          )
      )
  ));


  $result = new WP_Query();
  $result->posts = array_unique( array_merge( $q1->posts, $q2->posts ), SORT_REGULAR );
  $result->post_count = count( $result->posts );

  $customers = $result->posts;
} else {
  $customers = query_posts(array(
                            'post_type' => 'produtos',
                            's' => $_GET["s"],
                            'posts_per_page' => -1,
                            'meta_query' => $metaArray,
                        ));
}

// Valida as cores
$idPosts = array();
foreach ($customers as $key => $idPost) {
  $validar = get_post_meta($idPost->ID,'cor');
  $validarAplica = get_post_meta($idPost->ID,'aplicacao');
  if ($_GET['color'] != null || !empty($_GET['color'])) {
    foreach ($_GET['color'] as $key => $value) {
      $existeCor = in_array($value,$validar[0]);
      if ($_GET['aplicacao'] != null || !empty($_GET['aplicacao'])) {
        if ($existeCor) {
            $existeAplicacao = in_array($_GET['aplicacao'] ,$validarAplica[0]);
            if ($existeAplicacao) {
              $idPosts[$idPost->ID] = $idPost->ID;
            }
        }
      }else{
        if ($existeCor) {
          $idPosts[$idPost->ID] = $idPost->ID;
        }
      }
    }
  }else{
    if ($_GET['aplicacao'] != null || !empty($_GET['aplicacao'])) {
      $existeAplicacao = in_array($_GET['aplicacao'] ,$validarAplica[0]);
      if ($existeAplicacao) {
        $idPosts[$idPost->ID] = $idPost->ID;
      }
    }else{
      $idPosts[$idPost->ID] = $idPost->ID;
    }
  }
}

foreach ($idPosts as $var) {
  $pequisar[] = $var;
}

if (empty($pequisar)) {
  $pequisar[] = '9999999999999999'; //maneira de não bugar se o $pesquisar estivar null
}

$args = array(
  'post_type' => 'produtos',
  'posts_per_page' => -1,
  'post__in' => $pequisar
);

// Traz os resultados corretos
$post_certos = query_posts($args);

  $featured_image_id = 936;
  if ($featured_image_id) {
    $featured_small   = wp_get_attachment_image_src($featured_image_id, "featured-small" );
    $featured_medium  = wp_get_attachment_image_src($featured_image_id, "featured-medium" );
    $featured_large   = wp_get_attachment_image_src($featured_image_id, "featured-large" );
    $featured_xlarge  = wp_get_attachment_image_src($featured_image_id, "featured-xlarge" );

    $data_interchange = "data-interchange=\"" . $featured_small[0] . ", small], [" . $featured_medium[0] . ", medium], [" . $featured_large[0] . ", large], [" . $featured_xlarge[0] .", xlarge]\"";
  }

  
?>

<header id="featured-hero" role="banner" <?php echo $data_interchange; ?> class="small-header">
  <div class="featured-title">
      <h1>Resultado da pesquisa</h1>
  </div>
</header>
<?php get_template_part( 'template-parts/search' ); ?>
<div class="row">
  <div class="medium-12 columns">
    <?php foundationpress_breadcrumb(false,true); ?>
  </div>
</div>
  <div id="page-products" role="main">
    <article class="main-content">
          <h2>Produtos <strong>encontrados</strong></h2>
          <?php if ( have_posts() ) : ?>
            <div class="row small-up-1 medium-up-3 large-up-5 products-list">
              <?php while ( have_posts() ) : the_post(); ?>
                <div class="column column-block product-container">
                 <?php get_template_part( 'template-parts/content', "produtos" ); ?>
                </div>
              <?php endwhile; ?>
            </div>
          <?php else : ?>
            <?php get_template_part( 'template-parts/content', 'none' ); ?>
          <?php endif; // End have_posts() check. ?>
    </article>
    <?php get_sidebar(); ?>
  </div>

<?php get_footer();
